
// clear function for clearing fields in the form
clear = function(){
	$("#contactName").val("");
	$("#companyName").val("");
	$("#email").val("");
	$("#contactNumber").val("");
	$("#remarks").val("");
	$("#error-msg").html("");
}

emailValidate = function(){
	var regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var email = $("#email").val();

	if(!regexp.test(email)){
		return 1;
	}
	else{
		return 0;
	}
}

$(document).ready(function(){

	// Initially hide the Contact From from displaying 
	$('#contactForm').hide();
	// Display Contact Form when user click AddNew button
	$('#addNew').click(function(event){
		event.preventDefault();
		$('#contactForm').show();
	});

	// Get values from the form and append it to the Contact List table
	$('#save').click(function(event){
		event.preventDefault();
		contactName = 	$("#contactName").val();
		companyName = $("#companyName").val();
		email = $("#email").val();
		contactNumber = $("#contactNumber").val();
		remarks = $("#remarks").val();

		if(contactName == "" || companyName == "" || email == "" || contactNumber == "" || remarks == ""){
			$('#error-msg').html("<p style='color:red'>Enter all fields and proceed.</p>");
		}
		else {
			var validation = emailValidate();
			if(validation == 1){
				$('#error-msg').html("<p style='color:red'>Enter Valid email address.</p>");
			}
			else{
				var rowCount = $('#contactList tr').length;
				$('#contactList tr:last').after("<tr><td>"+rowCount+"</td><td>"+contactName+"</td><td>"+companyName+"</td><td>"+email+"</td><td>"+contactNumber+"</td><td>"+remarks+"</td><td><i class='fa fa-edit edit'></i>&nbsp;<i class='fa fa-trash delete'></i></td></tr>");
				$('#contactForm').hide();
				clear();
			}
		}
		
		
	});

	// clear all fields of form when clear button is clicked 
	$('#clear').click(function(event){
		event.preventDefault();
		clear();
	});


	// Hide Contact From from user, when cancel button is clicked 
	$('#cancel').click(function(event){
		event.preventDefault();
		clear();
		$('#contactForm').hide();
	});

	$('#deleteAll').click(function(event){
		event.preventDefault();
		$('#contactList tr:gt(0)').remove();
	});

	// delete cicked row 
	$('body').on('click','.delete',function(event){
        event.preventDefault();
		$(this).parent().parent().remove();
        count = 0;
		$('#contactList tr').each(function(){
			$(this).find("td:eq(0)").html(count);
			count++;
		});

		count = 0;
	});

	// change mouse style when hover the delete span
    $(document).on('mouseover', '.delete',  function () {
        $(this).css("cursor", "pointer");
    });

    // change mouse style when hover the edit span
    $(document).on('mouseover', '.edit',  function () {
        $(this).css("cursor", "pointer");
    });

    // edit
	$('body').on('click','.edit',function(){
	
		$("#contactName").val($(this).closest("tr").find('td:eq(1)').text());
		$("#companyName").val($(this).closest("tr").find('td:eq(2)').text());
		$("#email").val($(this).closest("tr").find('td:eq(3)').text());
		$("#contactNumber").val($(this).closest("tr").find('td:eq(4)').text());
		$("#remarks").val($(this).closest("tr").find('td:eq(5)').text());
		$('#contactForm').show();

	});

});